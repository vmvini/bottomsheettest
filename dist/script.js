// Code goes here
var $ = require("jquery");
require('jquery-ui-bundle');
require('angular');
require('angular-animate');
require('angular-aria');
require('angular-material');
require('./index.scss');


var app = angular.module('materialChips', ['ngMaterial']).
controller('AppController', function($scope, $mdBottomSheet) {
  //dsdsd
  $scope.showListBottomSheet = function($event) {
	    $mdBottomSheet.show({
	      //template: '<md-bottom-sheet class="md-grid md-has-header"><md-subheader>Share</md-subheader><md-list><md-list-item ng-repeat="item in items"><md-button class="md-grid-item-content" ng-click="listItemClick($index)"><i class="item.icon"></i><div class="md-grid-text"> {{ item.name }} </div></md-button></md-list-item></md-list></md-bottom-sheet>',
	      templateUrl: 'template.html',
	      controller: 'GridBottomSheetCtrl',
	      disableParentScroll: true,
	      disableBackdrop: true,
        clickOutsideToClose : true,
	      targetEvent: $event
	    }).then(function(clickedItem) {
	      //$scope.alert = clickedItem.name + ' clicked!';
	    });
	 };
  
})

.controller('GridBottomSheetCtrl', function($scope, $mdBottomSheet) {
  $scope.items = [
            { icon: 'fa-twitter', class: 'md-primary md-hue-2' },
            { icon: 'fa-google-plus', class: 'md-warn md-hue-2' },
            { icon: 'fa-facebook', class: 'md-primary' },
            { icon: 'fa-linkedin', class: 'md-primary' }
          ];
  $scope.listItemClick = function($index) {
    var clickedItem = $scope.items[$index];
    $mdBottomSheet.hide(clickedItem);
  };
});