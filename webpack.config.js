var webpack = require('webpack');
var path = require('path');
var MODE = process.env.MODE;


module.exports = {

  devServer: {
    port: 3000,
    historyApiFallback: true
  },

  entry: {
    app: path.resolve(__dirname, './src/script.js')
  },
  
  output: {
    path: path.resolve(__dirname, './dist/'),
    filename: '[name].js',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        //exclude: /(node_modules(?!\/(ui.leaflet.layers.webpack|ui.leaflet.webpack|ng-annotate))|bower_components)/,
        loaders: ['ng-annotate', 'babel-loader?presets[]=es2015']
      },
      { test: /\.json$/, loader: 'json' },

      { test: /\.css$/, loader: "style-loader!css-loader" },
      {
        test: /\.scss$/, // Only .css files
        loader: 'style!css!sass' // Run both loaders
      },

      {
        test: /\.woff(2)?(\?[a-z0-9]+)?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      }, 
      {
        test: /\.(ttf|eot|svg)(\?[a-z0-9]+)?$/,
        loader: "file-loader"
      },

      { 
        test: /\.svg$/, loader: 'svg-loader?{png:{scale:2}}'
      },
      {test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'}
    ]
  },

  plugins: [
 
      new webpack.ProvidePlugin({
        "$":"jquery",
        "jQuery":"jquery",
        "window.jQuery":"jquery"
      }),
      
      // OccurenceOrderPlugin: Assign the module and chunk ids by occurrence count. : https://webpack.github.io/docs/list-of-plugins.html#occurenceorderplugin
      new webpack.optimize.OccurenceOrderPlugin(),

      // Deduplication: find duplicate dependencies & prevents duplicate inclusion : https://github.com/webpack/docs/wiki/optimization#deduplication
      new webpack.optimize.DedupePlugin(),


  ]

};